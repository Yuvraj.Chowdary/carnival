import { Component, OnInit, OnDestroy } from "@angular/core";
import { NgForm } from "@angular/forms";
import { PostsService } from "../post.service";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Post } from "../post.model";
import { Subscription } from "rxjs";
@Component({
  selector: "app-post-creater",
  templateUrl: "./post-creater.component.html",
  styleUrls: ["./post-creater.component.css"],
})
export class PostCreaterComponent implements OnInit, OnDestroy {
  public enteredTitle = "";
  public enteredContent = "";
  public post: Post;
  private mode = "create";
  private postId: string;

  private subscriptions: Subscription = new Subscription();

  constructor(
    public postsService: PostsService,
    public route: ActivatedRoute
  ) {
    this.router
  }

  public ngOnInit() {
    this.allSubscriptions();
  }

  public ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public onSavePost(form: NgForm) {
    if (form.invalid) {
      return;
    } else {
      this.postsService.updatePost(
        this.postId,
        form.value.title,
        form.value.content
      );
    }

    this.postsService.addPost(form.value.title, form.value.content);
    form.resetForm();
  }

  public onAddPost(form: NgForm): void {
    if (form.invalid) {
      return;
    } else {
      this.postsService.addPost(form.value.title, form.value.content);
      form.resetForm();
    }
  }

  private allSubscriptions(): void {
    this.subscriptions.add(
      this.route.paramMap.subscribe((paramMap: ParamMap) => {
        if (paramMap.has("postId")) {
          this.mode = "edit";
          this.postId = paramMap.get("postId");
          this.post = this.postsService.getPost(this.postId);
        } else {
          this.mode = "create";
          this.postId = null;
        }
      })
    );
  }

}
