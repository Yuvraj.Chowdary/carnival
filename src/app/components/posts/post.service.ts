import { Post } from "./post.model";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { map } from 'rxjs/operators'
import { environment } from './../../../environments/environment';

@Injectable({ providedIn: "root" })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();
  private baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  getPosts() {
    this.http
      .get<{ message: string; posts: any }>(
       `${this.baseUrl}posts`
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
          return {
            title: post.title,
            content: post.content,
            id: post._id
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  getPost(id:String){
    return{...this.posts.find(p => p.id === id)};
  }
  addPost(title: string, content: string) {
    const post: Post = { id: null, title: title, content: content };
    this.http
      .post<{ message: string,postId: string }>(`${this.baseUrl}posts`, post)
      .subscribe((responseData) => {
        const id = responseData.postId;
        post.id = id;
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
      });
  }

updatePost(title:String,id: string,content:String){
 const post:Post = {title:title,id:id,content:content};
 this.http
     .put(`${this.baseUrl}posts`+id,post)
     .subscribe(response => console.log(response));
}
 
  deletePost(postId: string) {
    this.http.delete(`${this.baseUrl}posts` + postId)
      .subscribe(() => {
        const updatedPosts = this.posts.filter(post => post.id!==postId);
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
      }
      );
  }

}
