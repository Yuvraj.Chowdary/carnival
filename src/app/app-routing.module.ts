import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostListComponent } from './components/posts/post-list/post-list.component';
import { PostCreaterComponent } from './components/posts/post-creater/post-creater.component';


const routes: Routes = [
  {path:'',component:PostListComponent},
  {path:'create',component:PostCreaterComponent},
  {path:'edit/:postId',component:PostCreaterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
